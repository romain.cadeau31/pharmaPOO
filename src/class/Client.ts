export default class Client
{
    private nom: string;
    private credit: number;
    constructor(nom: string, credit: number)
    {
        this.nom = nom;
        this.credit = credit;
    }

    augmenterCredit(credit: number): void  // : void indique que cette methode ne renvoie rien
    {
        this.credit += credit;
    }

    diminuerCredit(credit: number): void // : void indique que cette methode ne renvoie rien
    {
        this.credit -= credit;
    }

    getCredit(): number // : number indique que cette methode renvoie un nombre
    {
        return this.credit;
    }

    getNom(): string // : string indique que cette methode renvoie une chaine de caractères
    {
        return this.nom;
    }
}