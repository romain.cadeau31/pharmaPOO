# Gestion de pharmacie en POO avec Typescript

## Instructions
* cloner le repo
* npm install
* npm start

Pas de page HTML, tout se fera via des console.log.
Les console.log s'affichent directement dans votre terminal.

---
# Consignes

* Analysez le fichier index.ts
* Analysez la class Main dans le dossier src/class.
* Analysez la class Client dans le dossier src/class.
--- 

### Completer la class Medicament dans le dossier src/class avec :
ces propriétés :
```
nom (private string)
prix (private number)
stock (private number)
```

et ces méthodes :
```
augmenterStock()
diminuerStock()
getStock()
getPrix()
getNom()
```
---

### Créer une class Vaccin qui extends Medicament avec :

cette propriété :

```
type (private string) designe la maladie que ce vaccin traite
```

et cette methode :
```
getType()
```
---

### Ajouter 2 clients et les afficher.
---
### Creer la methode ajouterMedicament dans Main, creez 3 medicaments et les afficher.
---
### Creer la methode ajouterVaccin dans Main, ajouter 2 vaccins et les afficher.
---
### Choisissez un client, et faites lui acheter un medicament. Vous devez donc creer une methode acheterMedicament dans Client. Pensez a actualiser le stock du medicament.
---
### Choisissez un client, et faites lui acheter un vaccin.
--- 
### Modifier la class Client pour permettre de savoir s'il est protégé contre une maladie.
---
### Dans Main, creez une methode qui renvoie la liste des Clients vaccinés
---
### Dans Main, creez une methode qui renvoie la liste des medicament dont le stock est à 0.
---
### Dans Main, creez une methode qui permet d'acheter des medicaments dont le stock est à 0. Acheter par lot de 10.
---
### Creez une class Utils avec une methode static nommée consoleArray() qui prend en parametre un array de type Medicament ou Client et qui fait juste un console.table() du parametre reçu. Faire une recherche sur la bonne utilisation des methodes static, et l'expliquer avec vos mots en commentaire dans ce fichier.
---
### Partout ou un console.log a été utilisé pour afficher une liste de medicaments ou de clients, remplacer ce console.log par Utils.consoleArray
---
### Cree un dossier models dans src, et ajoutez y un fichier IAdresse.ts avec ce contenu
```
export default interface IAdresse
{
    numero: number;
    rue: string;
    codePostal: number;
    ville: string;
}
```
---
### Ajouter a Client une propriete adresse de type IAdresse
---
### Modifiez la creation de vos clients pour leur ajouter des adresses de type IAdresse.
---
### Dans Client, rajoutez une methode getAdresse qui renvoie un objet de type IAdresse
---
### Reflechissez a la creation d'une class Pharmacie, ayant une adresse de type IAdresse, ainsi qu'une gestion de medicaments et de clients. Et l'implementer.
---
### Creez un fichier Personne.ts dans src/class :
```
export default abstract class Personne {
    abstract nom: string;
    abstract adresse: IAdresse;

    public abstract getNom(): string
}
```
---
### Creez un fichier PersonnelMedical dans src/class qui extends Personne
---
### Modifiez Client pour qu'il extends Personne
---
### Commentez le fichier Personne en expliquant l'utilité des classes abstraites
---
